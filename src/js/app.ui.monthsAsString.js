/**
 * Created by leandrola on 12/29/2017.
 */
document.app.ui.monthAsString = function (num) {
    var months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ];
    return months[num - 1]; // removes index 0, starts from 1
};

document.app.ui.getCurrentMonth = function () {
    var today = new Date();
    alert(document.app.ui.monthAsString(today.getMonth() + 1));
};
