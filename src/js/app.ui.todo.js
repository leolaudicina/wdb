/**
 * Created by leandrola on 12/29/2017.
 */
document.app.ui.todo = function () {
    var self = this;
    var todos = [],
        input = prompt("What would you like to do?");

    while (input !== "quit") {
        if (input === "list") {
            printTodoList();
        }
        else if (input === "new") {
            addTodo();
        }
        else if (input === "delete") {
            deleteTodo();
        }
        input = prompt("What would you like to do?");
    }

    function printTodoList() {
        var self = this;
        todos.forEach(getTodos);
    }

    /**
     * @name  getTodos()
     * @param todo  represents each element in the array (per loop iteration)
     * @param index represents the index of said element.
     */
    function getTodos(todo, index) {
        console.log(index + ": " + todo);
    }

    function addTodo() {
        var self = this;
        var newTodo = prompt("Enter a new todo");
        todos.push(newTodo);
    }

    function deleteTodo() {
        var self = this;
        var index = prompt("Enter index of todo to delete");
        todos.splice(index, 1);
    }
};
