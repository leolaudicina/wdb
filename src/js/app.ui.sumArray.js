document.app.ui.sumArray = function (a) {
    var self = this;

    if (a.length && typeof a === 'object') {
        var total = 0;

        a.forEach(clb);
        // Note: the plus sign that drops any "extra" zeroes at the end.
        +total.toFixed(1);

        return total;

        function clb(e) {
            total += e;
        }
    } else {
        console.log(arguments[0] + ' is not an Object.');
    }
};
