/**
 * Web Developer Bootcamp
 * Exercise Flow Control
 */
document.app.ui.ageCheck = function () {
    var age = prompt('your age to enter');

    if (age < 0) {
        console.log('you shall not pass!');
    }

    if (age >= 21) {
        console.log('drink you fool!');
    }

// If age is Odd
//(not evenly divisible by two)
    if (age % 2 !== 0) {
        console.log('Cool, you are odd man!');
    }

// If age is a perfect square
    if (age % Math.sqrt(age) === 0) {
        console.log('perfect Square!!');
    }

};
