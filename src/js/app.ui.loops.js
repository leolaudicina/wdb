/**
 * Web Developer Bootcamp
 * Exercise Loops
 */
document.app.ui.loopExercise1 = function () {
    console.log("PRINTING ALL NUMBERS BETWEEN -10 AND 19");
    var ex1 = -10;
    while (ex1 <= 19) {
        console.log(ex1);
        ex1++;
    }

    console.log("PRINTING ALL EVEN BETWEEN 10 AND 40");
    var ex2 = 10;
    var pares = ex2 % 2 === 0;

    while (ex2 <= 40) {
        if (pares) {
            console.log(ex2);
        }
        ex2 += 1;
    }

    // while (ex2 <= 40) {
    //     console.log(ex2);
    //     ex2 += 2;
    // }

    console.log("PRINTING ALL ODD NUMBERS BETWEEN 300 AND 333");
    var ex3 = 300;

    while (ex3 <= 333) {
        var impares = ex3 % 2 !== 0;

        if (impares) {
            console.log(ex3);
        }
        ex3 += 1;
    }

    console.log("PRINTING ALL NUMBERS DIVISIBLE BY 5 AND 3 BETWEEN 5 AND 50");
    var ex4 = 5;

    while (ex4 <= 50) {
        var divisiblePor = ex4 % 5 === 0 && ex4 % 3 === 0;

        if (divisiblePor) {
            console.log(ex4);
        }
        ex4 += 1;
    }
};

document.app.ui.loopExercise2 = function () {
    var answer = prompt("Are we there yet?"),
        notFound = answer.indexOf("yes") === -1;

    // indexOf() verifica que la str esté dentro del valor ingresado
    while (notFound) {
        answer = prompt("Are we there yet?");
    }

    swal({
        title: 'YAY, WE MADE IT!!!!',
        type: 'success',
        showConfirmButton: true
    })
};

document.app.ui.loopExercise3 = function () {
    console.log('PRINT ALL NUMBERS BETWEEN -10 AND 19');
    for (var n = -10; n <= 19; n++) {
        console.log(n);
    }

    console.log('PRINT OUT ALL EVEN NUMBERS BETWEEN 1 AND 50'); //PARES % 2 === 0
    for (var a = 0; a <= 50; a++) {
        var pares = a % 2 === 0;
        if (pares) {
            console.log(a);
        }
    }

    console.log('PRINT ALL ODD NUMBERS BETWEEN 300 AND 333'); //IMPARES % 2 !== 0
    for (var b = 300; b <= 333; b++) {
        var impares = b % 2 !== 0;
        if (impares) {
            console.log(b);
        }
    }

    console.log('PRINT ALL NUMBERS divisibles by 5 and 3 BETWEEN 5 AND 50');
    for (var c = 5; c <= 50; c++) {
        var divisiblePor = c % 5 === 0 && c % 3 === 0;
        if (divisiblePor) {
            console.log(c);
        }
    }

    console.log('WRITE CODE TO CREATE AN ASCII ART TRIANGLES LIKE THE ONE PICTURED. (NESTED LOOPS)');
    for (var d = 0; d < 6; d++) {
        for (var e = 0; e < d; e++) {
            console.log('$');
        }
        console.log('');
    }
    //SOLUTION: http://stackoverflow.com/questions/11409621/creating-a-triangle-with-for-loops
    /*
    $
    $$
    $$$
    $$$$
    $$$$$
    $$$$$$
    */
};

document.app.ui.loopExercise4 = function () {
//WRITE CODE TO CREATE AN ASCII ART TRIANGLES LIKE THE ONE PICTURED. (NESTED LOOPS)
    var i, j, k;

    for (i = 1; i < 10; i += 2) {
        for (k = 0; k < (4 - i / 2); k++) {
            console.log(" ");
        }
        for (j = 0; j < i; j++) {
            console.log("*");
        }
        console.log("");
    }
    /*
     *
     ***
     *****
     *******
     *********
     */
};

document.app.ui.loopExercise5 = function () {
    var numbers = [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9
    ];
    var colors = [
        'red',
        'green',
        'blue',
        'yellow',
        'aquamarine',
        'champagne',
        'wine',
        'bordeaux',
        'grape'
    ];
    numbers.forEach(function (color) {
        if (color % 3 === 0) {
            console.log(color);
        }
    });
};
