/**
 * Created by leandrola on 12/28/2017.
 */
//"to calculate the factorial of a number.
// For example, 5! = 5 x 4 x 3 x 2 x 1 = 120 "
document.app.ui.factorial = function (num) {
    var result = 1;

    if (typeof num === 'number') {
        for (var i = 2; i <= num; i++) {
            result *= i;
        }
    }
    return result;
};
