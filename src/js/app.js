'use strict';

var jq = jQuery;
document.app = {};
document.app.ui = {};

jq(document).ready(function () {
    var app = document.app;
    console.log('APP v1.0.0');
    console.log(app);
    app.retriveData();
});

/*
 * Grab data from file
 * @method      retriveData
 */
document.app.retriveData = function () {
    var dataSource = 'data/data.json';

    jq.getJSON(dataSource, document.app.renderDataVisualsTemplate);
};

/*
 * Render compiled handlebars template
 * @method      renderDataVisualsTemplate
 * @param       data
 */
document.app.renderDataVisualsTemplate = function (data) {
    document.app.handlebarsDebugHelper();
    document.app.renderHandlebarsTemplate('test', '#test', data);
    document.app.renderHandlebarsTemplate('app.ui.web-developer-bootcamp', '#webDeveloperBootcamp', data);
};

/*
 * Render handlebars templates via ajax
 *
 * @method      getTemplateAjax
 * @param       path to the partial
 * @param       callback  callback
 */
document.app.getTemplateAjax = function (path, callback) {
    var source, template;
    jq.ajax({
        url: path,
        success: function (data) {
            source = data;
            template = Handlebars.compile(source);
            if (callback) callback(template);
        }
    });
};

/*
 * Function to compile handlebars template
 *
 * @method      renderHandlebarsTemplate
 * @param       withTemplate template name
 * @param       inElement ID from HTML element
 * @param       withData  data to fill it
 */
document.app.renderHandlebarsTemplate = function (withTemplate, inElement, withData) {
    withData = withData || {};
    var route = 'partials/' + withTemplate + '.hbs';
    document.app.getTemplateAjax(route, function (template) {
        if (jq(inElement).data('template')) {
            jq(inElement).html(template(withData));
        }
    })
};

/*
 * Add handlebars debugger
 * @method      handlebarsDebugHelper
 */
document.app.handlebarsDebugHelper = function () {
    Handlebars.registerHelper("debug", function (optionalValue) {
        console.log("Current Context");
        console.log("====================");
        console.log(this);
    });
};
