document.app.ui.isUniform = function (a) {
    var self = this;

    if (a.length && typeof a === 'object') {
        var firstEl = a[0];

        for (var i = 1; i < a.length; i++) {
            if (a[i] !== firstEl) {
                return false;
            }
        }

        return true;

    } else {
        console.log(arguments[0] + ' is not an Object.');
    }
};
