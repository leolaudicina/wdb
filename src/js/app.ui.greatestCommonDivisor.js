/**
 * Created by leandrola on 12/28/2017.
 */
document.app.ui.gcd = function (num1, num2) {
    console.log("to find the greatest common divisor (gcd) of two positive numbers.");
    if (typeof num1 === "number" &&
        typeof num2 === "number") {
        if (!num2) {
            return num1;
        }
    }
    return getGCD(num2, num1 % num2);
};
