/**
 * Created by leandrola on 12/28/2017.
 */
document.app.ui.isEven = function (num) {
    console.log("calculate an even number");
    if (typeof num === 'number') {
        return num % 2 === 0;
    }
};
