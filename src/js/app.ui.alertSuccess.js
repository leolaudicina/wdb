/*
 * Se ejecuta mediante un handler en el DOM
 * y muestra un alert de Sweetalert2
 * @method      alertSuccess
 * @method      alertCheckoutSuccess
 */
document.app.ui.alertSuccess = function () {
    swal({
        title: '¡Listo!',
        html: '<p>Ya podés disfrutar de las recomendaciones que elegimos para vos.</p>',
        type: 'success',
        showConfirmButton: true
    }).then(function () {
        window.location.href = 'views/new.view.html';
    })
};