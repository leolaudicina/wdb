document.app.ui.reverseMe = function (a) {
    var self = this;

    if (a.length !== 0 && typeof a === 'object') {
        a.reverse();
        a.forEach(clb);
    }
    else {
        return arguments[0] + ' is not an Object.';
    }

    function clb(el, index) {
        return el + ':' + index;
    }
};
