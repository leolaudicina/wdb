/**
 * Created by leandrola on 12/28/2017.
 */
document.app.ui.capitalize = function (str) {
    if (typeof str === 'string') {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
};
