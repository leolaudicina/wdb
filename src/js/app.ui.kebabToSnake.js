/**
 * Created by leandrola on 12/28/2017.
 */
document.app.ui.kebabToSnake = function (str) {
    var regExp = /-/g;

    if (typeof str === 'string') {
        return str.replace(regExp, '_');
    }
    else {
        return false;
    }
};
