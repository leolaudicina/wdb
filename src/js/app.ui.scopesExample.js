/**
 * Created by leandrola on 12/29/2017.
 */
document.app.ui.outterScope = (function () {
    var foo = "Foobar!";

    var innerScope = function () {
        console.log(foo);
    };

    return innerScope;
})();
